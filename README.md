# SpreeRevoplus

This extension adds Revoplus payment to your Spree store.

## Installation

1) Add this extension to your Gemfile with this line:
      ```ruby
      gem 'spree_revoplus', github: 'RevoTechnologies/spree_revoplus', tag: 'v0.6.1'
      ```

2) Install the gem using Bundler:
      ```
      bundle install
      ```
3) Create `revo.yml` file in your `/config` folder with those parameters (development is just for example): 
    ```yaml
    staging:
       revo_url: '<revo_address>'
       revo_script: '<revo_address>/javascripts/iframe/v2/revoiframe.js'
       store_callback_url: '<your_shop_url>/api/v1/revo/notify'
       redirect_url: '<url_to_main_page_of_your_shop>'
       store_id_revo: <your_shop_id>
    development:
       revo_url: 'http://localhost:3000'
       revo_script: 'http://localhost:3000/javascripts/iframe/v2/revoiframe.js'
       store_callback_url: 'http://localhost:4000/api/v1/revo/notify'
       redirect_url: 'http://localhost:4000'
       store_id_revo: <your_shop_id>
    ```
4) Add to your `secrets.yml` following line:
    ```yaml
    staging:
        store_key: <your_store_key>
    ```
5) Add Revo payment method to `config/initializers/spree.rb`:
    ```ruby
    Rails.application.config.spree.payment_methods << Spree::PaymentMethod::Revo
    ``` 
## Endpoints

This gem adds few endpoints to your application. Use these endpoints on your views:

- `GET /api/v1/revo/register` - path to registration action, you can bind it to button on your page 

- `POST /api/v1/revo/notify` - callbacks from Revo will be delivered here, it's already configured

- `PUT /api/v1/revo/orders/:id/finish` - use this action when you want to finish order on Revo side, required params: `id`

- `PUT /api/v1/revo/orders/:id/cancel` - use this action when you want to cancel order on Revo side, required params: `id`

- `PUT /api/v1/revo/orders/:id/return` - use this action when you want to return any order amount on Revo side, required params: `id, amount`

Also, you can use generated routing helpers:

`GET api_v1_revo_register_path`

`POST api_v1_revo_notify_path`

`PUT finish_api_v1_revo_order_path(order.number)`

`PUT cancel_api_v1_revo_order_path(order.number)`

`PUT return_api_v1_revo_order_path(order.number)`

Example: `link_to 'Finish order', finish_api_v1_revo_order_path(order), method: :put`

## Internal API methods

For using API inside your code, like models, controllers,.. not views, you can use these API methods:

#### Bulk operations

##### Finish orders
```ruby
SpreeRevoplus::InternalApi::BulkOperation.new(orders, 'finish').call
```

##### Cancel orders
```ruby
SpreeRevoplus::InternalApi::BulkOperation.new(orders, 'cancel').call
```

##### Return orders
```ruby
SpreeRevoplus::InternalApi::BulkOperation.new(orders, 'return').call
```

Where `orders` is ActiveRecord objects which prepared for bulk operations

#### Notify store about order statuses
After sending bulk operations to Revoplus, your store can be received current order statuses. Revoplus sends info about orders to specific URL, 
which set up in store settings on Revoplus

##### Response samples which sends from Revoplus:

```ruby
{ order_id: '<order_id>', status: '<status>', message: '<message>' }
```

* **order_id** [string] - order number, like 'ORDER1' 
* **status** [string] - order status from Revoplus, available values - `finished, canceled, returned, error` 
* **message** [string] - service message about success operation or contains errors description.
* **signature** [string] - response signature from Revoplus.

##### How to handle Revoplus callback in our store?
Here is a sample of the controller, which handle Revoplus callback:

```ruby
class RevoCallbackController < ApplicationController
  skip_before_action :verify_authenticity_token
  before_action :validate_signature
  before_action :load_details

  def create
    if params[:status] == 'error'
      # NOTE: Processing errors
    else
      # NOTE: Processing statuses finished, canceled, return
    end
    render body: 'ok', status: :ok
  end

  private

  def validate_signature
    unless SpreeRevoplus::Utils::SignatureValidator.valid?(params)
      raise(StandardError, 'Invalid signature!')
    end
  end

  def load_details
    order = Spree::Order.find_by(number: params[:order_id])
    @payment = order.payments.last
  end
end
```
**Important note**: Please, validate the signature, it helps you to protect your store from unknown and insecure requests.

And don't forget to add the route to your routes.rb
```ruby
post :revo_callback, to: 'revo_callback#create'
```

## Translations

This gem adds Polish translations by default, you can edit them if necessary.

```
pl:
  spree:
    order_finish_revo: "Zamówienie pomyślnie potwierdzone w Revo"
    order_finish_revo_error: "Wystąpił błąd podczas potwierdzania zamówienia w Revo"
    order_cancel_revo: "Zamówienie pomyślnie anulowane w Revo"
    order_cancel_revo_error: "Wystąpił błąd podczas anulowania zamówienia w Revo"
    order_return_revo: "Zwrot w Revo został poprawnie wykonany"
    order_return_revo_error: "Wystąpił błąd podczas wykonywania zwrotu w Revo"
```

## Payment

After proper installation you should be able to add new payment type called "Revo" to your shop using admin panel.

## iFrame

Payment with Revo and registration endpoint will render Revoplus page using iFrame.