# frozen_string_literal: true

module Spree
  module Api
    module V1
      module Revo
        class OrdersController < Spree::BaseController
          before_action :load_order, only: %i[finish cancel return]
          skip_before_filter :verify_authenticity_token

          def finish
            service = SpreeRevoplus::FinishRequestRevo.new(@order).call
            check_response(service)
            back_redirect
          end

          def cancel
            service = SpreeRevoplus::CancelRequestRevo.new(@order).call
            check_response(service)
            back_redirect
          end

          def return
            service = SpreeRevoplus::ReturnRequestRevo.new(@order, order_params[:amount]).call
            check_response(service)
            back_redirect
          end

          private

          def load_order
            @order = Spree::Order.find_by!(number: order_params[:id])
          end

          def back_redirect
            redirect_back(fallback_location: spree.edit_admin_order_url(@order))
          end

          def order_params
            params.permit(:amount, :id)
          end

          def check_response(service)
            parsed_response = JSON.parse(service.body)
            if parsed_response['status'].zero?
              flash[:success] = Spree.t("order_#{action_name}_revo")
            else
              flash[:error] = Spree.t("order_#{action_name}_revo_error", message: parsed_response['message'])
            end
          end
        end
      end
    end
  end
end