# frozen_string_literal: true

module Spree
  module Api
    module V1
      module Revo
        class CallbacksController < Spree::BaseController
          protect_from_forgery except: %i[notify]

          def notify
            order = Spree::Order.find_by(number: params[:order_id])
            @payment = order.payments.last

            if revo_payment? && need_update_state?
              if params[:decision] == 'approved'
                @payment.complete!
              elsif params[:decision] == 'declined'
                @payment.failure!
              end
            end

            render body: 'ok', status: :ok
          end

          private

          def revo_payment?
            @payment.payment_method.is_a?(Spree::PaymentMethod::Revo)
          end

          def need_update_state?
            %w[completed failed processing].exclude?(@payment.state)
          end
        end
      end
    end
  end
end