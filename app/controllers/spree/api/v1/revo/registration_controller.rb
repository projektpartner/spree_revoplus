# frozen_string_literal: true

module Spree
  module Api
    module V1
      module Revo
        class RegistrationController < ApplicationController
          def register
            request = SpreeRevoplus::RegistrationRequestRevo.new(spree_current_user)
            response = JSON.parse(request.call.body)
            if response['status'].zero?
              @redirect_url = Rails.application.config_for(:revo)['redirect_url']
              @url = response['iframe_url']
              respond_to do |f|
                f.html { render 'revo/iframe.html.erb'}
                f.json { render json: { url: @url } }
              end
            end
          end
        end
      end
    end
  end
end
