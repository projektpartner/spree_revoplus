# frozen_string_literal: true

module Spree
  CheckoutController.class_eval do
    before_action :pay_with_revo, only: :update

    def pay_with_revo
      return unless params[:state] == 'payment'
      return if params[:order].blank? || params[:order][:payments_attributes].blank?

      pm_id = params[:order][:payments_attributes].first[:payment_method_id]
      payment_method = Spree::PaymentMethod.find(pm_id)

      return unless payment_method&.is_a?(Spree::PaymentMethod::Revo)

      request = SpreeRevoplus::CheckoutRequestRevo.new(@order, order_url(@order))
      @response = JSON.parse(request.call.body)
      if @response['status'].zero?
        @url = @response['iframe_url']
        @redirect_url = order_url(@order)
        render 'revo/iframe.html.erb' if payment_success(payment_method)
      else
        revo_error
      end
    end

    private

    def payment_success(payment_method)
      payment = @order.payments.build(
        payment_method_id: payment_method.id,
        amount: @order.total,
        state: 'checkout'
      )

      unless payment.save && @order.next
        flash[:error] = payment.errors.full_messages.join("\n")
        redirect_to checkout_state_path(@order.state) and return # rubocop:disable Style/AndOr
      end

      payment.pend!
    end

    def revo_error
      @order.errors[:base] << "Revo error: #{@response['message']}"
      render :edit
    end
  end
end
