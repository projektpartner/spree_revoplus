# frozen_string_literal: true

class Spree::PaymentMethod::Revo < Spree::PaymentMethod
  def provider_class
    Spree::PaymentMethod::Revo
  end

  def source_required?
    false
  end

  def success?
    true
  end

  def method_type
    'revo'
  end
end
