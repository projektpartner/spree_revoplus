# frozen_string_literal: true

module SpreeRevoplus
  class FinishPayloadRevo
    def initialize(order)
      @order = order
    end

    def call
      payload
    end

    private

    def payload
      {
        order_id: @order.number,
        amount: format('%.2f', @order.total.to_f),
        check_number: @order.number
      }
    end
  end
end
