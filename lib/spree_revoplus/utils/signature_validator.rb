module SpreeRevoplus
  module Utils
    class SignatureValidator
      def self.valid?(params)
        payload = params.slice('order_id', 'status', 'message').to_json
        Digest::SHA1.hexdigest(payload + Rails.application.secrets.store_key) == params[:signature]
      end
    end
  end
end

