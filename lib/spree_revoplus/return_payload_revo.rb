# frozen_string_literal: true

module SpreeRevoplus
  class ReturnPayloadRevo
    def initialize(order, amount)
      @order_id = order.number
      @amount = amount
    end

    def call
      payload
    end

    private

    def payload
      {
        order_id: @order_id,
        amount: format('%.2f', @amount.to_f)
      }
    end
  end
end
