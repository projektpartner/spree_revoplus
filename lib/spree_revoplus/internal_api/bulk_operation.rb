# frozen_string_literal: true

module SpreeRevoplus
  module InternalApi
    class BulkOperation
      def initialize(orders, bulk_operation)
        @orders = orders
        @bulk_operation = bulk_operation
      end

      def call
        send_request
      end

      private

      attr_reader :orders, :bulk_operation

      def endpoint
        '/factoring/v1/pre_check/bulk_operations'
      end

      def request
        @request ||= SendRequestRevo.new(endpoint, payload)
      end

      def send_request
        request.call
      end

      def payload
        { orders: payload_orders, bulk_operation: bulk_operation }
      end

      def payload_orders
        orders.map do |order|
          { order_id: order.number, amount: format('%.2f', order.total.to_f) }
        end
      end
    end
  end
end
