# frozen_string_literal: true

require 'digest/sha1'

module SpreeRevoplus
  class SendRequestRevo
    def initialize(endpoint, data)
      @endpoint = endpoint
      @data = data
    end

    def call
      send_request
    end

    private

    def connection
      @connection ||= Faraday.new(url: config['revo_url'])
    end

    def config
      @config ||= Rails.application.config_for(:revo)
    end

    def store_id
      config['store_id_revo']
    end

    def signature
      secret_key = Rails.application.secrets.store_key
      Digest::SHA1.hexdigest(@data.to_json + secret_key)
    end

    def payload
      @data.to_json
    end

    def send_request
      connection.post do |req|
        req.url "#{@endpoint}?store_id=#{store_id}&signature=#{signature}"
        req.headers['Content-Type'] = 'application/json'
        req.body = payload
      end
    end
  end
end
