# frozen_string_literal: true

module SpreeRevoplus
  class CancelPayloadRevo
    def initialize(order)
      @order = order
    end

    def call
      payload
    end

    private

    def payload
      { order_id: @order.number }
    end
  end
end
