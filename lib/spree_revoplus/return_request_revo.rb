# frozen_string_literal: true

module SpreeRevoplus
  class ReturnRequestRevo
    def initialize(order, amount)
      @order = order
      @amount = amount
    end

    def call
      send_request
    end

    private

    def endpoint
      'factoring/v1/return'
    end

    def data
      @data ||= ReturnPayloadRevo.new(@order, @amount).call
    end

    def request
      @request ||= SendRequestRevo.new(endpoint, data)
    end

    def send_request
      request.call
    end
  end
end
