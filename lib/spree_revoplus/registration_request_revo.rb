# frozen_string_literal: true

module SpreeRevoplus
  class RegistrationRequestRevo
    def initialize(user)
      @user = user
    end

    def call
      send_request
    end

    private

    def endpoint
      'factoring/v1/limit/auth'
    end

    def data
      @data ||= RegistrationPayloadRevo.new(@user).call
    end

    def request
      @request ||= SendRequestRevo.new(endpoint, data)
    end

    def send_request
      request.call
    end
  end
end
