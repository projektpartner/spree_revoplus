# frozen_string_literal: true

module SpreeRevoplus
  class RegistrationPayloadRevo
    def initialize(user)
      @user = user
    end

    def call
      payload
    end

    private

    def payload
      {
        callback_url: Rails.application.config_for(:revo)['store_callback_url'],
        redirect_url: Rails.application.config_for(:revo)['redirect_url'],
        primary_email: @user ? @user.email : '',
        primary_phone: '',
        current_order: {
          order_id: (0...8).map { rand(65..90).chr }.join
        },
        person: {
          first_name: '',
          surname: ''
        },
        skip_result_page: false
      }
    end
  end
end
