# frozen_string_literal: true

module SpreeRevoplus
  class CheckoutRequestRevo
    def initialize(order, redirect_url)
      @order = order
      @redirect_url = redirect_url
    end

    def call
      send_request
    end

    private

    def endpoint
      'factoring/v1/pre_check/auth'
    end

    def data
      @data ||= OrderPayloadRevo.new(@order, @redirect_url).call
    end

    def request
      @request ||= SendRequestRevo.new(endpoint, data)
    end

    def send_request
      request.call
    end
  end
end
