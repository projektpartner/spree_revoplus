# frozen_string_literal: true

module SpreeRevoplus
  class FinishRequestRevo
    def initialize(order)
      @order = order
    end

    def call
      send_request
    end

    private

    def endpoint
      'factoring/v1/pre_check/finish'
    end

    def data
      @data ||= FinishPayloadRevo.new(@order).call
    end

    def request
      @request ||= SendRequestRevo.new(endpoint, data)
    end

    def send_request
      request.call
    end
  end
end
