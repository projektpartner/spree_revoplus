# frozen_string_literal: true

module SpreeRevoplus
  class ProductsDecoratorRevo
    def initialize(order)
      @order = order
    end

    def call
      build_hash
    end

    private

    def build_hash
      attrs = %i[name price quantity]
      products.pluck(*attrs).map { |p| attrs.zip(p).to_h }
    end

    def products
      @products ||= order.products
    end

    attr_accessor :order
  end
end
