# frozen_string_literal: true

module SpreeRevoplus
  class OrderPayloadRevo
    def initialize(order, redirect_url)
      @order = order
      @redirect_url = redirect_url
    end

    def call
      payload
    end

    private

    def payload
      {
        callback_url: Rails.application.config_for(:revo)['store_callback_url'],
        redirect_url: @redirect_url,
        primary_email: @order.email,
        primary_phone: @order.bill_address.phone,
        current_order: {
          amount: format('%.2f', @order.total.to_f),
          order_id: @order.number,
          bill_address: @order.bill_address
        },
        person: {
          first_name: @order.bill_address.firstname,
          surname: @order.bill_address.lastname
        },
        skip_result_page: false,
        cart_items: product_list
      }
    end

    def product_list
      ProductsDecoratorRevo.new(@order).call
    end
  end
end
