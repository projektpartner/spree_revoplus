Spree::Core::Engine.add_routes do
  namespace :api, defaults: { format: 'json' } do
    namespace :v1 do
      namespace :revo do
        post :notify, controller: 'callbacks'
        get :register, controller: 'registration'

        resources :orders, only: [] do
          member do
            put :finish
            put :cancel
            put :return
          end
        end
      end
    end
  end
end
